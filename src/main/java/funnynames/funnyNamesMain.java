package funnynames;

import utils.Colors;

import static java.lang.System.*;

class FunnyNamesMain {

    public static void main(String[] args) {
        FunnyNames funnyNames = new FunnyNames();
        for (int i = 0; i < 5; i++) {
            String firstName = funnyNames.getFirstName();
            String lastName = funnyNames.getLastName();
            out.println(Colors.valueOf("ANSI_GREEN") + lastName + ", " + firstName + Colors.valueOf("ANSI_RESET"));
        }
    }
}
