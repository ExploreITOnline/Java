package funnynames;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class FunnyNames {

    private final Random rand = new Random();
    public String getFirstName() {
        List<String> availableFirstNames = fillFirstNames();
        int randomIndex = rand.nextInt(availableFirstNames.size());
        return availableFirstNames.get(randomIndex);
    }

    public String getLastName() {
        List<String> availableLastNames = fillLastNames();
        int randomIndex = rand.nextInt(availableLastNames.size());
        return availableLastNames.get(randomIndex);
    }

    public List<String> fillFirstNames(){
        return Arrays.asList("Josh", "Andy", "Becky");
    }

    public List<String> fillLastNames(){
        return Arrays.asList("Smith", "Anderson", "Witten");
    }
}


