package tests;

import funnynames.FunnyNames;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class TestFunnyNames{
    FunnyNames funnyNames = new FunnyNames();
    FunnyNames fakeNames = spy(funnyNames);

    @Test
    public void testGetFirstName(){
        when(fakeNames.fillFirstNames()).thenReturn(Collections.singletonList("Will"));
        assertEquals("Will", fakeNames.getFirstName(), "Method 'getFirstName()' returns unexpected value");
    }

    @Test
    public void testGetLastName(){
        List<String> fakeLastNames = Arrays.asList("Smith", "Weber");
        when(this.fakeNames.fillLastNames()).thenReturn(fakeLastNames);
        assertNotEquals(-1, fakeLastNames.indexOf(fakeNames.getLastName()), "Method 'getLastName()' returns unexpected value");
    }
}
